************
Installation
************

Installation is simple two step process.

#. Install the package::

    pip3 install sphinxcontrib-constdata

#. Append |project| to the list of extensions in your ``conf.py``::

        extensions = [
            ...
            'sphinxcontrib.constdata'
        ]
