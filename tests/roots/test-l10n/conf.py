# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath("../.."))

# -- Project information -----------------------------------------------------

project = "Refdata test project"
copyright = "2020, Documatt"
author = "Documatt"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ["sphinxcontrib.constdata"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "basic"


# -- Options for sphinxcontrib.constdata extension -----------------------

menu_template = {
    "label": ":menuselection:`{_('Path')}`",
    "link": ":menuselection:`{_('Path')}`",
}

conf_template = {
    "id": "_('Variable')",
    "label": "*{_('Category')}*",
    "link": "``{_('Variable')}``",
}

constdata_files = {
    "menu_gettext.csv": menu_template,
    "menu_gettext.json": menu_template,
    "menu_gettext.yaml": menu_template,
    "conf_gettext.csv": conf_template,
    "conf_gettext.json": conf_template,
    "conf_gettext.yaml": conf_template,
    "conf_gettext_id_col_not_first.yaml": conf_template,
}
