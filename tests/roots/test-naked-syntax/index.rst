Naked (default domain)
======================

Full syntax
-----------

.. constdata:table:: menu.csv

Please go to :constdata:label:`menu.csv?FileNew`.

Please go to :constdata:link:`menu.csv?FileSaveAs`.

Naked syntax
------------

.. default-domain:: constdata

.. table:: menu.csv

Please go to :label:`menu.csv?FileNew`.

Please go to :link:`menu.csv?FileSaveAs`.