import os
import re
from pathlib import Path
from typing import Callable, Iterable, Optional, Tuple

from babel.messages import Catalog
from babel.messages.pofile import write_po
from sphinx.builders.gettext import MessageCatalogBuilder
from sphinx.locale import init
from sphinx.util import logging

from sphinxcontrib.constdata.flatfiles import FlatfileReader, rglob_flatfiles_from
from sphinxcontrib.constdata.settings import (
    CONFIG_POT_COMMENTS,
    CONFIG_POT_LOCATION,
    CONFIG_POT_LOCATION_FILE,
    CONFIG_POT_LOCATION_FILE_RECORD,
    CONFIG_POT_LOCATION_NO,
    CONFIG_POT_MSGCTXT,
    Settings,
)
from sphinxcontrib.constdata.utils import ConstdataError

logger = logging.getLogger(__name__)


CONSTDATA_ROOT_CATALOG = "constdata"
"""Messages from flatfiles"""

CONSTDATA_EXTENSION_CATALOG = "sphinxcontrib-constdata"
"""Messages of the sphinxcontrib-constdata extension itself"""


def gettext(
    settings: Settings, rel_path: str
) -> Callable[[Optional[str], Optional[str]], str]:
    """
    Creates and returns function that for an original (msgid), returns a translation (msgstr).

    To support msgctxt, the function also needs rel_path ("file"
    placeholder).

    The returning function is

        fn(translatable_string, id_col_val) -> str

    Translatable string is between ``_(`` and ``)``). If it is not found, returns the original string.

    Depending on settings, a search is limited to msgctxt. If msgctxt is used,
    the rel_path of this function and id_col_value passed to returning function
    will be used to generate the same context value as was used during
    creating POT file. Header row has ``None`` id_col_value.

    Usage::

        rel_path = "menu.csv"
        _ = gettext(settings, rel_path)
        expected = "Soubor --> Uložit jako..."
        actual = _("_('File --> Save As...')", "FileSaveAs")
        assert expected == actual
    """

    # abs dirs of all locales dirs (e.g. locales/en/)
    dirs = [
        os.path.join(settings.env.srcdir, directory)
        for directory in settings.env.config.locale_dirs
    ]
    catalog, has_catalog = init(
        dirs, settings.env.config.language, CONSTDATA_ROOT_CATALOG
    )

    # with open(mo_file, "rb") as f:
    #     catalog: Catalog = read_mo(f)

    msgctxt_template = settings.get_pot_msgctxt()

    def _(message: Optional[str], id_col_value: Optional[str] = None):
        # is it translatable message? I.e. contains _('')
        if not message:
            return message

        match = msg_pattern.match(message)
        if not match:
            return message

        msgid = match.group("msg")

        if not has_catalog:
            return msgid

        # Either simple gettext(), no context consulted
        if msgctxt_template is None:
            msgstr = catalog.gettext(msgid)
            return msgstr

        # Or, use pgettext() that restrict translation to given context
        context = resolve_msgctxt_template(msgctxt_template, rel_path, id_col_value)
        msgstr = catalog.pgettext(context, msgid)
        return msgstr

    return _


# Valid messages examples:

# _('''Minim nostrud elit aute Lorem aliquip occaecat do eu.
#
# Duis nulla laborum Lorem fugiat voluptate. Cupidatat sit cupidatat ullamco et exercitation. Lorem sit qui consequat ea id commodo non fugiat amet.
#
# * Culpa pariatur quis esse elit officia eiusmod sit.
# * Cillum sit ad tempor cillum proident.''')

# _('Cillum sit ad tempor cillum proident.')

# _("Cillum sit ad tempor cillum proident.")
# _("""Cillum sit ad tempor
# cillum proident""")
msg_pattern = re.compile(
    r"""
     ^                               # beginning of string
     _\(                             # starts with _(
     (?P<quote>'''|'|\"\"\"|\")      # start-string is triple/single '/"
     (?P<msg>.+)                     # message to translate
     (?P=quote)                      # same end-string as was start-string
     \)                              # expression ends with )
     $                               # end of string
 """,
    re.VERBOSE | re.DOTALL,
)


def resolve_msgctxt_template(
    template: str, file: str, id_col_value: Optional[str]
) -> str:
    """
    Helper that resolves msgctxt template.

    :raises ConstdataError: if template contains unknown placeholder.
    """
    from sphinxcontrib.constdata.templating import resolve_template2

    # Handle if msgctxt template is invalid value
    try:
        resolved = resolve_template2(
            template,
            {
                "file": file,
                # None would become "None", but it should be ""
                "id_col_value": id_col_value or "",
            },
        )
        return resolved

    except KeyError:
        raise ConstdataError(
            f"The value '{template}' of conf.py variable {CONFIG_POT_MSGCTXT} is invalid. Available keys: file, id_col_value."
        )


class FlatfileMessageCatalogBuilder(MessageCatalogBuilder):
    """
    Runs unmodified Sphinx builtin gettext builder, after it extract messages from constdata_root folder.
    """

    def finish(self) -> None:
        # run original gettext builder
        super().finish()

        # e.g. /path/to/sphinx/_build/gettext/constdata.pot
        abs_pot_path = Path(self.outdir, f"{CONSTDATA_ROOT_CATALOG}.pot")

        catalog = Catalog(
            project=self.config.project,
            version=self.config.version,
            copyright_holder=self.config.author,
        )
        env = self.app.env
        assert env
        settings = Settings(env)
        flags: tuple = settings.get_pot_flags()

        for msg, location, recordno, comment, id_col_value in self._extract_messages():
            catalog.add(
                id=msg,
                context=self._render_context(location, id_col_value),
                string=None,
                locations=self._render_location(location, recordno),
                auto_comments=(comment,),
                flags=flags,
            )

        with open(abs_pot_path, "wb") as f:
            # e.g. constdata.pot
            rel_pot_path = abs_pot_path.relative_to(self.outdir)
            logger.info(f"writing constdata catalog {rel_pot_path}")

            write_po(
                f,
                catalog,
                # no line wrapping
                width=0,
            )

    def _render_context(self, file, id_col_value) -> Optional[str]:
        """Build value of msgctxt for a message"""
        env = self.app.env
        assert env
        settings = Settings(env)
        template_string = settings.get_pot_msgctxt()

        # Falsy value means, no msgctxt
        if not template_string:
            return None

        return resolve_msgctxt_template(template_string, file, id_col_value)

    def _render_location(self, location, recordno):
        """Returns list of tuples as expected by Babel's Catalog.add()"""
        # https://babel.pocoo.org/en/latest/api/messages/catalog.html#babel.messages.catalog.Catalog.add
        env = self.app.env
        assert env
        settings = Settings(env)
        loc_type = settings.get_pot_location()

        if loc_type == CONFIG_POT_LOCATION_FILE:
            return [(location, None)]
        elif loc_type == CONFIG_POT_LOCATION_FILE_RECORD:
            return [
                (
                    location,
                    recordno,
                )
            ]
        elif loc_type == CONFIG_POT_LOCATION_NO:
            return []
        else:
            raise ConstdataError(
                f"The value '{loc_type}' of conf.py variable {CONFIG_POT_LOCATION} is invalid."
            )

    def _render_header_comment(self, file):
        from sphinxcontrib.constdata.templating import resolve_template2

        env = self.app.env
        assert env
        settings = Settings(env)
        template_string = settings.get_pot_comments()[0]

        try:
            return resolve_template2(template_string, {"file": file})
        except KeyError as ex:
            raise ConstdataError(
                f"The header template string '{template_string}' of conf.py variable {CONFIG_POT_COMMENTS} is invalid. Available keys: file.",
                orig_exc=ex,
            )

    def _render_nonheader_comment(self, location, id_col_name, id_col_value):
        from sphinxcontrib.constdata.templating import resolve_template2

        env = self.app.env
        assert env
        settings = Settings(env)
        template_string = settings.get_pot_comments()[1]

        try:
            return resolve_template2(
                template_string,
                {
                    "file": location,
                    "id_col_name": id_col_name,
                    "id_col_value": id_col_value,
                },
            )
        except KeyError as ex:
            raise ConstdataError(
                f"The non-header template string '{template_string}' of conf.py variable {CONFIG_POT_COMMENTS} is invalid. Available keys: location.",
                orig_exc=ex,
            )

    def _extract_messages(self) -> Iterable[Tuple[str, str, int, str, Optional[str]]]:
        """
        Search flatfiles for translatable messages and returns iterable where
        each item is (message, filepath, recordno, extracted_comment,
        id_col_val) tuple. The header row has ``None`` ID column value.

        Extracted comment is a comment directed at the translator with the instructions that might help understand the translated string.
        """
        translatables = []
        env = self.app.env
        assert env
        settings = Settings(env)
        flatfiles = rglob_flatfiles_from(settings.get_root())

        for abs_path in flatfiles:
            rel_path = abs_path.relative_to(settings.get_root())

            # relative path to the root, e.g. "entities/User.yaml"
            location = str(rel_path)

            flatfile = FlatfileReader(settings, rel_path)
            for i, row in enumerate(flatfile.iterate_rows()):
                # pick header from first row
                if i == 0:
                    for col_name in row.keys():
                        match = msg_pattern.match(str(col_name))
                        if match:
                            msg = match.group("msg")
                            comment = self._render_header_comment(location)
                            translatables.append((msg, location, i, comment, None))

                # first and further rows
                for col_val in row.values():
                    match = msg_pattern.match(str(col_val))
                    if match:
                        msg = match.group("msg")
                        id_col_name = settings.get_id_col_name(rel_path)
                        id_col_value = row[id_col_name]
                        comment = self._render_nonheader_comment(
                            location, id_col_name, id_col_value
                        )

                        translatables.append(
                            (msg, location, i + 1, comment, id_col_value)
                        )

        return translatables
