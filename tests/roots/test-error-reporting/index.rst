Error reporting
===============

Non-existing table
------------------

.. constdata:table:: nonexisting.csv

Please see :constdata:label:`nonexisting.csv?nonexisting`.

Please see :constdata:link:`nonexisting.csv?nonexisting`.

Missing template
----------------

Please see :constdata:label:`menu.csv?nonexisting`.

Please see :constdata:link:`menu.csv?nonexisting`.

.. cannot test

   Table without id column set
   ---------------------------

   .. constdata:table:: conf.json

   because it stops build

Some table
----------

.. constdata:table:: shortcuts.json

Nonexisting column in templates
-------------------------------

Column "PC" not exist (it is "pc"):

.. inline label templates not supported

.. Please go to :constdata:label:`:kbd:\'{PC}\'/:kbd:\'{mac}\' (Mac) <shortcuts.json?FileSaveAs>`.

Please go to :constdata:link:`:kbd:\'{PC}\'/:kbd:\'{mac}\' (Mac) <shortcuts.json?FileSaveAs>`.

Nonexisting row
---------------

Please see :constdata:label:`shortcuts.json?nonexisting`.

Please see :constdata:link:`shortcuts.json?nonexisting`.

Same table again
----------------

.. constdata:table:: shortcuts.json