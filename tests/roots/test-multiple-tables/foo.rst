Foo page
========

List a table
------------

.. constdata:table:: conf.json

Label and link
--------------

Please see :constdata:label:`conf.json?project_copyright`.

Please see :constdata:link:`conf.json?project_copyright`.