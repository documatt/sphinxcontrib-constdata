Links to the same documents
===========================

.. toctree::

   second
   third
   fourth
   fifth

.. constdata:table:: conf2.csv

Link to table from the same document (expected_link1.html):

:constdata:link:`conf2.csv?project`

Link to table from the same document with custom title (expected_link2.html):

:constdata:link:`The documented project’s name <conf2.csv?project>`